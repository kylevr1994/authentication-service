package com.blotux.authentication.classes;

import javax.persistence.*;

@Entity
@Table(name = "account_restores")
public class AccountRestore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="account_id")
    private String account_id;

    @Column(name="reset_code")
    private String reset_code;

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getReset_code() {
        return reset_code;
    }

    public void setReset_code(String reset_code) {
        this.reset_code = reset_code;
    }

    @Id
    public Long getId() {
        return id;
    }
}
