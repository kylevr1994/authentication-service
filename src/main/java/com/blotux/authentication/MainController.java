package com.blotux.authentication;


import com.blotux.authentication.classes.Account;
import com.blotux.authentication.classes.ResetAccount;
import com.blotux.authentication.classes.Session;
import com.blotux.authentication.mail.EmailSenderService;
import com.blotux.authentication.repositories.AccountRawSQLRepository;
import com.blotux.authentication.repositories.AccountRepository;
import com.blotux.authentication.repositories.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.security.SecureRandom;

@Component
@RestController
public class MainController {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    AccountRawSQLRepository accountRawSQLRepository;

    @Autowired
    EmailSenderService senderService;

    @RequestMapping(value = "/test")
    public String test(){
        return "The service has started correctly";
    }



    @RequestMapping(value = "/forgot-password")
    public boolean forgotPassword(@RequestBody Account account) {

        // check if a valid email is given
        if(account.getEmail() == null || account.getEmail() == "") { return false; }

        // check if the email address exists in the database
        Account userAccount = accountRepository.findByEmail(account.getEmail());
        if(userAccount.getEmail() == null){ return false; };
        if(userAccount.getId() == null){ return false; };

        // create a restore password code in the database
        String resetCode = accountRawSQLRepository.insertAccountRestore(userAccount.getId());

        // send email with reset code to the user
        senderService.sendEmail(account.getEmail(),"Perfect Poster password reset","Your reset code is " + resetCode);

        return true;
    }

    @RequestMapping(value = "/reset-password")
    public boolean resetPassword(@RequestBody ResetAccount resetAccount) {

        // check if a valid email is given

        // check if the resetCode is correct

        // change the account password to the new given password

        return false;
    }

    /**
     * Create a new account
     * @param account
     * @return
     */
    @RequestMapping(value = "/register")
    public Session register(@RequestBody Account account) {
        try {

            // Check if the needed values are given
            if(account.getEmail() == null) { return null; }
            if(account.getPassword() == null) { return null; }
            if(account.getName() == null) { return null; }

            // Check if the account already exists
            if(accountRepository.findByEmail(account.getEmail())!=null){
                return null;
            }

            // Encrypt the given password with the added salt (build in the BCrypt)
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
            String encodedPassword = encoder.encode(account.getPassword());
            account.setPassword(encodedPassword);

            // Insert the new account in to the database
            accountRawSQLRepository.insertAccount(account);

            // Get account from the database to see if it is existing
            Account userAccount = accountRepository.findByEmail(account.getEmail());

            // create and return a session
            Session session = createSession(userAccount);
            return session;

        } catch (Exception ex) {
            return null;
        }
    }



    /**
     * Checks if the user has an account and submitted the right data. If so create a sessionKey that can be used to
     * access the application. Standard the session key will expire the next day.
     * @param account email and password
     * @return service sessionKey
     */
    @RequestMapping(value = "/login")
    public Session login(@RequestBody Account account) {

        try {
            // Check if the needed values are given
            if(account.getEmail() == null){ return null; }
            if(account.getPassword() == null){ return null; }

            // Get account from the database
            Account userAccount = accountRepository.findByEmail(account.getEmail());

            // Encrypt the given password with the added salt (build in the BCrypt)
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
            String encodedPassword = encoder.encode(account.getPassword());

            // Check if the encrypted passwords match
            if (encoder.matches(account.getPassword(), userAccount.getPassword())) {

                Session session = createSession(userAccount);
                // Get full session information out of the database
                return session;
            }
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Check if the user has access to the services by checking if the user has the correct accessToken/UserKey (Id).
     * @param auth the accessToken provided in the header.
     * @param userId the userKey provided in the header.
     * @return true if the auth/key are correct, false if not.
     */
    @RequestMapping(value = "/token_check")
    public Boolean tokenCheck(@RequestHeader(value = "Authorization") String auth, @RequestHeader(value = "UserId") Long userId){

        try{
            if(!auth.isEmpty() && userId != null){
                Boolean session = sessionRepository.existsBySessionKeyAndUserId(auth, userId);
                if(session){
                    return true;
                }
            }
            return false;
        }
        catch (HttpClientErrorException.BadRequest exception){
            return null;
        }
    }

    private Session createSession(Account account) {

        // Create a random session key
        SecureRandom secureRandom = new SecureRandom();
        long randomLong = secureRandom.nextLong();
        String sessionKey = account.getId().toString() + "Code" + randomLong; //TODO: add current date to the session

        // Create new session
        Session session = new Session();
        session.setSessionKey(sessionKey); // set the session key
        session.setUserId(account.getId()); // set the user id
        session.setId(0000L);   // set the id, this will be changed in the database

        // Save session in the database
        sessionRepository.save(session);

        return sessionRepository.findBySessionKeyAndUserId(session.getSessionKey(), session.getUserId());
    }

}
