package com.blotux.authentication.repositories;

import com.blotux.authentication.AuthenticationApplication;
import com.blotux.authentication.classes.Account;
import com.blotux.authentication.classes.ResetAccount;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Random;

@Repository
public class AccountRawSQLRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void insertAccount(Account account) {
        entityManager.createNativeQuery("INSERT INTO accounts (`name`, `email`, `password`) VALUES (? , ?, ?)")
                .setParameter(1, account.getName())
                .setParameter(2,account.getEmail())
                .setParameter(3, account.getPassword())
                .executeUpdate();
    }

    @Transactional
    public String insertAccountRestore(Long accountId) {

        // generate a secure random code
        String generatedCode = getSecureRandomString(5);

        entityManager.createNativeQuery("INSERT INTO account_restores (`reset_code`, `account_id`) VALUES (?, ?)")
                .setParameter(1,generatedCode)
                .setParameter(2,accountId)
                .executeUpdate();

        return generatedCode;
    }


    public String getSecureRandomString(int length){
        String SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SecureRandom secureRandom = new SecureRandom();
        StringBuilder sb = new StringBuilder(length); for (int i = 0; i < length; i++) sb.append(SOURCE.charAt(secureRandom.nextInt(SOURCE.length())));
        return sb.toString();
    }




}
