package com.blotux.authentication.repositories;

import com.blotux.authentication.classes.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findAccountByEmailAndPassword(String email, String password);

    Account findByEmail(String email);

}
