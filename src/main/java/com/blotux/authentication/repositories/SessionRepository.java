package com.blotux.authentication.repositories;

import com.blotux.authentication.classes.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {

    Session findBySessionKeyAndUserId(String sessionKey, Long userId);

    Boolean existsBySessionKeyAndUserId(String sessionKey, Long userId);
}
