package com.blotux.authentication;

import com.blotux.authentication.mail.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Random;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class AuthenticationApplication {

	//@Autowired
	//private EmailSenderService senderService;

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationApplication.class, args);

	}
//	@EventListener(ApplicationReadyEvent.class)
//	public void sendMail(){
//
	//}







}
