# Authentication service 

**Note:** this is an old project where I was learning how to use Spring Boot. I wanted to create my own
authentication service. There are a lot of things that I would do differently now, but I'm keeping this
project as a reminder of my progress. This application used to make a connection with a AWS RDS database.

for example:
- In this project I never return an error message to the user, but just a null value. In the Trivia backend app
  you can see how I do this the proper way.
- Recovery has a 1 in 100000 chance of failing because the number generation that I implemented
  is random, this was for testing purposes.
- Naming and structure of the project is not the best, but ok.
- No hardcoded passwords, I would use a better way to store them now.

In other project that I am currently (2024) working on I use Spring security and JWT tokens to authenticate users. 

<br>

## How to run the project
<b>Build executable file command:</b> mvn clean package <br>
<b> Run command: </b> mvn spring-boot:run <br>
<b> Build Docker image: </b> docker build -t perfectposter/authentication:latest . <br>
<b> Run Docker image: </b>docker run -d -p 8080:8081 perfectposter/authentication:latest 
