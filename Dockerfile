FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /opt/app

ARG JAR_FILE=target/authentication-1.0.jar
COPY ${JAR_FILE} authentication-1.0.jar

ENTRYPOINT ["java","-jar","authentication-1.0.jar"]

